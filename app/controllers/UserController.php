<?php

class UserController extends BaseController
{
	// Gets the view for the register page
	public function getCreate()
	{
		return View::make('user.register');
	}

	// gets the view for the login page
	public function getLogin()
	{
		return View::make('user.login');
	}

	// takes a post form for the create page
	public function postCreate()
	{
		$validate = Validator::make(Input::all(), array(
			'username' => 'required|unique:users|min:4',
			'password' => 'required|min:6',
			'confirm' => 'required|same:password',
		));

		if ($validate->fails())
		{
			return Redirect::route('getCreate')->withErrors($validate)->withInput();
		}
		else
		{
			$user = new User();
			$user->username = Input::get('username');
			$user->password = Hash::make(Input::get('password'));

			if ($user->save())
			{
				return Redirect::route('home')->with('success', 'You registered successfully! You can now log in!');
			}
			else
			{
				return Redirect::route('home')->with('fail', 'An error occured while creating the user. Please try again.');
			}
		}
	}

	// takses a post form for the login page
	public function postLogin()
	{
		$validator = Validator::make(Input::all(), array(
				'username' => 'required',
				'password' => 'required'
			));

		if ($validator->fails())
		{
			return Redirect::route('getLogin')->withErrors($validator)->withInput();
		}
		else
		{
			$remember = (Input::has('remember')) ? true : false;

			$auth = Auth::attempt(array(
					'username' => Input::get('username'),
					'password' => Input::get('password')
				), $remember);

			if ($auth)
			{
				return Redirect::intended('/');
			}
			else
			{
				return Redirect::route('getLogin')->with('fail', 'Username and/or password is incorrect.');
			}
		}
	}

	public function getLogout()
	{
		Auth::logout();
		return Redirect::route('home');
	}
}



