{{-- Imports the layout --}}
@extends('layouts.master') 

{{-- attaches to the @section tag and imports this content to the layout --}}
@section('head')
	@parent {{-- imports "super" --}}
	<title>Home Page</title>
@stop {{-- marks where the section import ends --}}

{{-- Finds the section (in this case 'yield') named 'content' --}}
@section('content')
	Home Page
@stop {{-- marks the end of the section import. --}}